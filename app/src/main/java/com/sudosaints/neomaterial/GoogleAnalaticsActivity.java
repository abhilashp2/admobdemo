package com.sudosaints.neomaterial;

import android.app.Activity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.HashMap;


public class GoogleAnalaticsActivity extends ActionBarActivity {

	public enum TrackerName {
		GLOBAL_TRACKER,
	}

	private HashMap<TrackerName, Tracker> mTrackers;
	private Activity activity;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_google_analatics);
		activity = this;
		mTrackers = new HashMap<TrackerName, Tracker>();
		Tracker t = getTracker(TrackerName.GLOBAL_TRACKER);
		t.setScreenName("Temp Screen");
		t.send(new HitBuilders.AppViewBuilder().build());
	}


	@Override
	public void onStop() {
		super.onStop();
		GoogleAnalytics.getInstance(activity).reportActivityStop(activity);
	}

	@Override
	public void onStart() {
		super.onStart();
		GoogleAnalytics.getInstance(activity).reportActivityStart(activity);
	}

	public synchronized Tracker getTracker(TrackerName trackerId) {
		if (!mTrackers.containsKey(trackerId)) {
			GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
			Tracker t = analytics.newTracker(getString(R.string.ga_tracking_id));
			mTrackers.put(trackerId, t);
		}
		return mTrackers.get(trackerId);
	}
}