package com.sudosaints.neomaterial;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;


public class MainActivity extends ActionBarActivity {

	@InjectView(R.id.material_label_button)
	FloatingActionButton button;
	@InjectView(R.id.adView)
	AdView adView;

	private InterstitialAd interstitialAd;
	private Activity activity;
	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ButterKnife.inject(this);
		activity = this;
		interstitialAd = new InterstitialAd(activity);
		interstitialAd.setAdUnitId(getResources().getString(R.string.admob_id));
		/**
		 * Below code used when we release application
		 * AdRequest adRequest = new AdRequest.Builder().build();
		 */


		/**
		 * Below code used ad developer option
		 */
		//AdRequest adRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).addTestDevice("").build();
		adView.loadAd(new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).addTestDevice("").build());
		interstitialAd.setAdListener(new AdListener() {
			@Override
			public void onAdLoaded() {
				displayInterstitial();
			}
		});
	}

	public void displayInterstitial() {
		if (interstitialAd.isLoaded()) {
			interstitialAd.show();
		}
	}


	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, activity, PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				// Log.i(TAG, "This device is not supported.");
				Toast.makeText(activity, "This device is not supported.", Toast.LENGTH_SHORT).show();
				finish();

			}
			return false;
		}
		return true;
	}


	@OnClick(R.id.material_label_button)
	public void onFloatButtinClicked() {
		startActivity(new Intent(activity, GoogleAnalaticsActivity.class));
	}
}
